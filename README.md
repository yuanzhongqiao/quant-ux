<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a href="https://github.com/KlausSchaefers/quant-ux/actions/workflows/docker.yml"><img src="https://github.com/KlausSchaefers/quant-ux/actions/workflows/docker.yml/badge.svg" alt="Docker 镜像构建并推送到 Dockerhub - CI/CD" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quant-UX - 原型、测试和学习</font></font></h1><a id="user-content-quant-ux---prototype-test-and-learn" class="anchor" aria-label="永久链接：Quant-UX - 原型、测试和学习" href="#quant-ux---prototype-test-and-learn"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quant UX 是一种研究、可用性和原型设计工具，可快速测试您的设计并获得数据驱动的见解。该存储库包含前端。您可以在</font><a href="https://quant-ux.com/#/" rel="nofollow"><font style="vertical-align: inherit;">https://quant-ux.com/#/</font></a><font style="vertical-align: inherit;">找到工作演示</font></font><a href="https://quant-ux.com/#/" rel="nofollow"><font style="vertical-align: inherit;"></font></a></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="https://github.com/KlausSchaefers/quant-ux/blob/master/docs/preview.jpg?raw=true"><img src="https://github.com/KlausSchaefers/quant-ux/raw/master/docs/preview.jpg?raw=true" alt="替代文本" title="Quant-UX 预览" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发设置</font></font></h2><a id="user-content-develpment-setup" class="anchor" aria-label="永久链接：开发设置" href="#develpment-setup"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm install
</code></pre><div class="zeroclipboard-container">
   
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于开发的编译和热重载</font></font></h3><a id="user-content-compiles-and-hot-reloads-for-development" class="anchor" aria-label="永久链接：编译和热重载以进行开发" href="#compiles-and-hot-reloads-for-development"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm run serve
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编译并缩小以用于生产</font></font></h3><a id="user-content-compiles-and-minifies-for-production" class="anchor" aria-label="永久链接：编译并缩小以用于生产" href="#compiles-and-minifies-for-production"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm run build
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行您的单元测试</font></font></h3><a id="user-content-run-your-unit-tests" class="anchor" aria-label="永久链接：运行单元测试" href="#run-your-unit-tests"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm run test:unit
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lint 和修复文件</font></font></h3><a id="user-content-lints-and-fixes-files" class="anchor" aria-label="永久链接：Lints 和修复文件" href="#lints-and-fixes-files"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm run lint
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h1><a id="user-content-installation" class="anchor" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自行安装并运行的最简单方法是使用</font></font><a href="https://github.com/bmcgonag"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Brian McGonagill</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供的预构建 Docker 映像。您可以在</font><a href="https://github.com/bmcgonag/quant-ux-docker/"><font style="vertical-align: inherit;">https://github.com/bmcgonag/quant-ux-docker/</font></a><font style="vertical-align: inherit;">找到存储库和说明</font></font><a href="https://github.com/bmcgonag/quant-ux-docker/"><font style="vertical-align: inherit;"></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动安装</font></font></h2><a id="user-content-manual-installation" class="anchor" aria-label="永久链接：手动安装" href="#manual-installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quant-UX 有两个组件。前端（此包）和后端（qux-java）。前端需要安装 Node.js (&gt; 12)。后端需要 Mongo DB、邮件服务器 (SMTP) 和 Java (&gt; 1.8)。前端配备了自己的迷你 Web 服务器，其中还包括一个代理，可将所有请求重定向到正确的后端。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">码头工人</font></font></h2><a id="user-content-docker" class="anchor" aria-label="永久链接：Docker" href="#docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行您自己的 Quant-UX 安装的最简单方法是使用 Docker 映像。</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建一个 docker compose 文件 ( </font></font><code>docker-compose.yaml</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">) 并设置环境变量。</font></font></li>
</ol>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-ent">version</span>: <span class="pl-s"><span class="pl-pds">'</span>3<span class="pl-pds">'</span></span>

<span class="pl-ent">services</span>:
  <span class="pl-ent">mongo</span>:
    <span class="pl-ent">restart</span>: <span class="pl-s">always</span>
    <span class="pl-ent">container_name</span>: <span class="pl-s">quant-ux-mongo</span>
    <span class="pl-ent">image</span>: <span class="pl-s">mongo</span>
    <span class="pl-ent">volumes</span>:
      - <span class="pl-s">./data:/data/db        </span><span class="pl-c"><span class="pl-c">#</span> pth for the data to be stored and kept on your host machine is on the left side of the ":"</span>
  <span class="pl-ent">qux-fe</span>:
    <span class="pl-ent">restart</span>: <span class="pl-s">always</span>
    <span class="pl-ent">container_name</span>: <span class="pl-s">quant-ux-frontend</span>
    <span class="pl-ent">image</span>: <span class="pl-s">klausenschaefersinho/quant-ux</span>
    <span class="pl-ent">environment</span>:
      - <span class="pl-s">QUX_PROXY_URL=http://quant-ux-backend:8080        </span><span class="pl-c"><span class="pl-c">#</span> this is the path the front end uses to talk tot he backend</span>
      - <span class="pl-s">QUX_AUTH=qux</span>
      - <span class="pl-s">QUX_KEYCLOAK_REALM=</span>
      - <span class="pl-s">QUX_KEYCLOAK_CLIENT=</span>
      - <span class="pl-s">QUX_KEYCLOAK_URL=</span>
      - <span class="pl-s">QUX_WS_URL=ws://127.0.0.1:8086        </span><span class="pl-c"><span class="pl-c">#</span> change to where the websocket server is deployed for external access</span>
    <span class="pl-ent">links</span>:
      - <span class="pl-s">mongo</span>
      - <span class="pl-s">qux-be</span>
    <span class="pl-ent">ports</span>:
      - <span class="pl-c1">8082:8082</span>        <span class="pl-c"><span class="pl-c">#</span> change the left side port if your host machine already has 8082 in use</span>
    <span class="pl-ent">depends_on</span>:
      - <span class="pl-s">qux-be</span>
  <span class="pl-ent">qux-be</span>:
    <span class="pl-ent">restart</span>: <span class="pl-s">always</span>
    <span class="pl-ent">container_name</span>: <span class="pl-s">quant-ux-backend</span>
    <span class="pl-ent">image</span>: <span class="pl-s">klausenschaefersinho/quant-ux-backend</span>
    <span class="pl-ent">volumes</span>:
      - <span class="pl-s">./quant-ux-data:/app-data</span>
    <span class="pl-ent">environment</span>:
      - <span class="pl-s">QUX_HTTP_HOST=http://quant-ux-frontend:8082   </span><span class="pl-c"><span class="pl-c">#</span> this is the URL included in the mails, e.g. password resets</span>
      - <span class="pl-s">QUX_HTTP_PORT=8080  </span><span class="pl-c"><span class="pl-c">#</span> This is the port the backend will use</span>
      - <span class="pl-s">QUX_MONGO_DB_NAME=quantux  </span><span class="pl-c"><span class="pl-c">#</span> the database / collection name in mongodb</span>
      - <span class="pl-s">QUX_MONGO_TABLE_PREFIX=quantux  </span><span class="pl-c"><span class="pl-c">#</span> table / document prefix in mongodb</span>
      - <span class="pl-s">QUX_MONGO_CONNECTION_STRING=mongodb://quant-ux-mongo:27017 </span><span class="pl-c"><span class="pl-c">#</span> this assumes your mongodb container will be called "quant-ux-mongo" in the docker-compose file</span>
      - <span class="pl-s">QUX_MAIL_USER=mail_admin@example.com        </span><span class="pl-c"><span class="pl-c">#</span> this should be your smtp email user</span>
      - <span class="pl-s">QUX_MAIL_PASSWORD=sTr0ngPa55w0Rd        </span><span class="pl-c"><span class="pl-c">#</span> this should be your smtp email password</span>
      - <span class="pl-s">QUX_MAIL_HOST=mail.example.com        </span><span class="pl-c"><span class="pl-c">#</span> this should be your smtp host address</span>
      - <span class="pl-s">QUX_JWT_PASSWORD=some-long-string-of-mix-case-chars-and-nums        </span><span class="pl-c"><span class="pl-c">#</span> you should change this to a real JWT secret</span>
      - <span class="pl-s">QUX_IMAGE_FOLDER_USER=/app-data/qux-images        </span><span class="pl-c"><span class="pl-c">#</span> this folder should mapped in the volume</span>
      - <span class="pl-s">QUX_IMAGE_FOLDER_APPS=/app-data/qux-image-apps        </span><span class="pl-c"><span class="pl-c">#</span> this folder should mapped in the volume</span>
      - <span class="pl-s">TZ=America/Chicago        </span><span class="pl-c"><span class="pl-c">#</span> change to your timezone</span>
      - <span class="pl-s">QUX_AUTH_SERVICE=qux</span>
      - <span class="pl-s">QUX_KEYCLOAK_SERVER= </span><span class="pl-c"><span class="pl-c">#</span> just the keycloak host &amp; port</span>
      - <span class="pl-s">QUX_KEYCLOAK_REALM=</span>
      - <span class="pl-s">QUX_USER_ALLOW_SIGNUP=true </span><span class="pl-c"><span class="pl-c">#</span> set the false to not allow users to signup</span>
      - <span class="pl-s">QUX_USER_ALLOWED_DOMAINS=* </span><span class="pl-c"><span class="pl-c">#</span> comma separated list of domains, e.g. 'my-server.com' or '*' for all</span>
    <span class="pl-ent">depends_on</span>:
      - <span class="pl-s">mongo</span>
  <span class="pl-ent">qux-ws</span>:
    <span class="pl-ent">restart</span>: <span class="pl-s">always</span>
    <span class="pl-ent">container_name</span>: <span class="pl-s">quant-ux-websocket-server</span>
    <span class="pl-ent">image</span>: <span class="pl-s">klausenschaefersinho/quant-ux-websocket</span>
    <span class="pl-ent">environment</span>:
      - <span class="pl-s">QUX_SERVER=http://quant-ux-backend:8080/</span>
      - <span class="pl-s">QUX_SERVER_PORT=8086</span>
    <span class="pl-ent">ports</span>:
      - <span class="pl-c1">8086:8086</span>
    <span class="pl-ent">links</span>:
      - <span class="pl-s">qux-be</span>
    <span class="pl-ent">depends_on</span>:
      - <span class="pl-s">qux-be</span>
</pre><div class="zeroclipboard-container">
 
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请务必更新</font></font><code>QUX_JWT_PASSWORD</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ENV 变量以确保您的安装安全。更新</font></font><code>QUX_HTTP_HOST</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><code>QUX_MAIL_USER</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><code>QUX_MAIL_PASSWORD</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并</font></font><code>QUX_MAIL_HOST</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保正确的邮件处理</font></font></p>
<ol start="2" dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用以下命令启动容器</font></font></li>
</ol>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker compose up</pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一键部署</font></font></h2><a id="user-content-one-click-deployment" class="anchor" aria-label="永久链接：一键部署" href="#one-click-deployment"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">埃莱斯蒂奥</font></font></h3><a id="user-content-elestio" class="anchor" aria-label="永久链接：埃莱斯蒂奥" href="#elestio"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您只需点击几下鼠标并在您选择的云服务提供商上进行最少的配置即可部署 Quant UX 实例。</font></font></p>
<p dir="auto"><a href="https://elest.io/open-source/quant-ux" rel="nofollow"><img src="https://camo.githubusercontent.com/20bc2e60414176e6de7a9244ba478f2b32997f0e3f21526022f7f32a35126914/68747470733a2f2f7075622d64613336313537633835343634383636393831336633663736633532366332622e72322e6465762f6465706c6f792d6f6e2d656c657374696f2d626c61636b2e706e67" alt="部署" data-canonical-src="https://pub-da36157c854648669813f3f76c526c2b.r2.dev/deploy-on-elestio-black.png" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">库伯内斯</font></font></h2><a id="user-content-kubernets" class="anchor" aria-label="永久链接：Kubernetes" href="#kubernets"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以在这里找到 kubernets 配置</font></font><a href="https://github.com/engmsilva/quant-ux-k8s/tree/master/k8s"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/engmsilva/quant-ux-k8s/tree/master/k8s</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">后端</font></font></h3><a id="user-content-backend" class="anchor" aria-label="永久链接：后端" href="#backend"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 Mongo DB (&gt; 4.4)</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 Java (1.8)</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">检查后端</font></font></p>
</li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>git clone https://github.com/KlausSchaefers/qux-java.git
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这已经在发布文件夹中包含了后端的编译版本</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编辑 matc.conf 文件以设置正确的 mongo 和邮件服务器详细信息。更多详细信息可以在这里找到：</font></font><a href="https://github.com/KlausSchaefers/qux-java"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/KlausSchaefers/qux-java</font></font></a></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启动服务器，或在 Linux 中安装为服务。</font></font></p>
</li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>java -jar release/matc.jar -Xmx2g -conf matc.conf -instances 1
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="java -jar release/matc.jar -Xmx2g -conf matc.conf -instances 1" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前端</font></font></h3><a id="user-content-front-end" class="anchor" aria-label="永久链接：前端" href="#front-end"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 Node.js (&gt; 12)</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克隆仓库</font></font></p>
</li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>git clone https://github.com/KlausSchaefers/quant-ux.git
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装所有依赖项：</font></font></li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm install
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建造</font></font></li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>npm run build
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配置前端</font></font></h3><a id="user-content-config-front-end" class="anchor" aria-label="永久链接：配置前端" href="#config-front-end"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将代理服务器 url 设置为 ENV 变量</font></font></li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>export QUX_PROXY_URL=https://your.quant-ux.server.com // backend host

export QUX_WS_URL= wss.quant-ux.server.com // web socket server

</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开始</font></font></li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>node server/start.js
</code></pre><div class="zeroclipboard-container">
 
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反向代理</font></font></h3><a id="user-content-reverse-proxy" class="anchor" aria-label="永久链接：反向代理" href="#reverse-proxy"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在你应该有一个正在运行的系统了。目前还不安全。最好的办法是将两者都放在处理 SSL 的 NGINX 反向代理后面。</font></font></p>
<ul dir="auto">
<li><a href="https://www.scaleway.com/en/docs/tutorials/nginx-reverse-proxy/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.scaleway.com/en/docs/tutorials/nginx-reverse-proxy/</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以使用</font></font><a href="https://letsencrypt.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://letsencrypt.org/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建 SSL 证书</font></font></p>
</article></div>
